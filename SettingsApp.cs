﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json.Serialization;
using System.Xml.Linq;

namespace Test_Client
{
    /// <summary>
    /// Класс настроек приложения
    /// </summary>
    public class SettingsApp
    {
        /// <summary>
        /// Список серверов для подключения
        /// </summary>
        public List<Server> @Servers;
        public Listen @Listen;
        public SettingsApp(string filename)
        {
            using (StreamReader r = new StreamReader(filename))
            {
                var readJson = JsonFileReader.Read<ServersSettings>(filename);
                @Servers = readJson.Servers;
                Listen = readJson.Listen;
            }
        }
    }
    
    public static class JsonFileReader
    {
        public static T Read<T>(string filePath)
        {
            string text = File.ReadAllText(filePath);
            return JsonSerializer.Deserialize<T>(text);
        }
    }

    public class ServersSettings
    {
        public List<Server> Servers { get; set; }
        public Listen @Listen { get; set; }
    }

    /// <summary>
    /// Список полученых пакетов
    /// </summary>
    public class ListReceive  
    {
        public string value { get; set; }
        public int status { get; set; }

        public ListReceive(string value, int status)
        {
            this.value = value;
            this.status = status;
        }
    }

    /// <summary>
    /// Класс расширяющий основные настрйоки сервера
    /// </summary>
    public class ServerStatus : Server   
    {
        public object locker = new object();
        public bool connected;
        public List<ListReceive> listReceive;
        public List<string> queueSend;

        public ServerStatus(Server server)
        {
            this.port = server.port;
            this.ip = server.ip;
            this.name = server.name;

            listReceive = new List<ListReceive>();
            queueSend = new List<string>();
        }
    }

    public class Server // Настройки подключения к серверу
    {
        public string name { get; set; }
        public string ip { get; set; }
        public int port { get; set; }
       
    }

    public class Client
    {
        public string name;// ip+port клиента
        public bool active; //Активность: подключен/уже отпал
        public List<string> queueSend; // Очередь отправки
        public Client()
        {
            queueSend = new List<string>();
        }
    }

    public class ListenStatus : Listen
    {
        public object locker = new object();
        public List<Client> clients;
        public ListenStatus(Listen listen)
        {
            this.port = listen.port;
            clients = new List<Client>();
        }
    }

    public class Listen
    {
        public int port { get; set; }
    }
}
