﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Test_Client
{
    public class Analize
    {
        const string startPacket = "#90";
        const string serviceInf = "#010102#27";
        const string endPacket = "#91";
        private List<ServerStatus> _serverStatus;
        private ListenStatus _listenstatus;
        private bool _terminate = false;

        private static Analize instance;

        public static Analize getInstance(List<ServerStatus> serverStatus, ListenStatus listenStatus)
        {
            if (instance == null)
                instance = new Analize(serverStatus, listenStatus);
            return instance;
        }

        public Analize(List<ServerStatus> serverStatus, ListenStatus listenStatus)
        {
            _serverStatus = serverStatus;
            _listenstatus = listenStatus;
            Execute();
        }

        public void Stop() 
        {
            _terminate = true;
        }

        private string GetStringBetweenSubString(string input, string strFrom, string strTo)
        {
            int posFrom = input.IndexOf(strFrom);
            if (posFrom != -1) 
            {
                posFrom += strFrom.Length;
                int posTo = input.IndexOf(strTo, posFrom + 1);
                if (posTo != -1) 
                {
                    return input.Substring(posFrom, posTo - posFrom);
                }
            }

            return null;
        }

        private void Execute()
        {
            Thread thread = new Thread(() =>
            {
                List<ListReceive> allRecServers = new List<ListReceive>();
                while (!_terminate)
                {
                    foreach (var item in _serverStatus)
                    {
                        if (item.connected)
                        {
                            lock (item.locker)
                            {
                                var rec = item.listReceive.Where(x => x.status == 0)?.FirstOrDefault();
                                if (rec != null)
                                    allRecServers.Add(rec);
                            }
                        }
                    }

                    if (allRecServers.Count == _serverStatus.Count) // Если данные пришли от всех серверов можно придумать ответ
                    {
                        List<string> Data1 = new List<string>();
                        List<string> Data2 = new List<string>();
                        string sendData1 = "", sendData2 = "";
                        foreach (var rec in allRecServers)
                        {
                            // Очень грубые проверки на формат присланных данных. Такую надо делать сразу при приёмке. (В задании только корректные)
                            var subs = GetStringBetweenSubString(rec.value, startPacket + serviceInf, endPacket)?.Split(';');
                            if (subs != null && subs.Length == 2)
                            {
                                Data1.Add(subs[0]);
                                Data2.Add(subs[1]);
                            }
                        }
                        if (Data1.Count == _serverStatus.Count)// Если получили "условно" корректные данные
                        {
                            sendData1 = (Data1.Distinct().Count() == 1) ? Data1.First() : "NoRead";
                            sendData2 = (Data2.Distinct().Count() == 1) ? Data2.First() : "NoRead";
                        }
                     
                        foreach (var item in _listenstatus.clients.Where(x=> x.active)) // Каждому клиенту выдаем сообщение
                        {
                            lock (_listenstatus.locker)
                            {
                                if (!string.IsNullOrEmpty(sendData1))
                                {
                                    item.queueSend.Add($"{startPacket}{serviceInf}{sendData1};{sendData2}{endPacket}\r\n");
                                }
                                else
                                {
                                    item.queueSend.Add("error");
                                    // Можно ченить ответить что формат сообщения не верный у кого то
                                }                              
                            }
                        }
                        int i = 0;
                        foreach (var item in _serverStatus) // Меняем статус сообщениям котроые обработали
                        {
                            lock (item.locker) 
                            {
                                allRecServers[i].status = 1;
                            }
                            i++;
                        }    
                    }
                                                  
                    allRecServers.Clear();
                    Thread.Sleep(10);
                }
            });
            thread.IsBackground = true;
            thread.Start();
        }


    }
}
