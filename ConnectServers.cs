﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace Test_Client
{
    /// <summary>
    /// Класс подключения и обмена сообщениями с серверами
    /// </summary>
    public class ConnectServers  // :IDisposable освобождать не будем для теста
    {
        private static ConnectServers instance;

        public static ConnectServers getInstance(List<Server> settings)
        {
            if (instance == null)
                instance = new ConnectServers(settings);
            return instance;
        }

        private Thread _exchange;
        public List<ServerStatus> serverStatus = new List<ServerStatus>();
        private bool _terminate = false;

        //Если таки надо делать управление потоками то их надо добавлять в List<Thread> каждый подключенный сервер, опустим это. Живем в идеальном мире тестового задания)

        protected ConnectServers(List<Server> settings)
        {
            foreach (Server server in settings)
            {
                ServerStatus curItem = new ServerStatus(server);
                serverStatus.Add(curItem);
            }
            Connect();
        }

        /// <summary>
        /// Сообщения в консоль
        /// </summary>
        private void SendMeesage(string mes)
        {
            Console.WriteLine(mes);
        }

        public void Disconnect()
        {
            _terminate = true;
        }
        private void Connect()
        {
            Thread thread = new Thread(() =>  // Поток следит за состоянием подключения - если отпал пытается цеплятся заново
            {                
                while (!_terminate)
                {
                    foreach (ServerStatus item in serverStatus)
                    {
                        if (!item.connected)
                        {      
                            ServerConnect(item);  // Тут Async/Await не делаем Пусть подключает все сервера
                        }
                    }
                    Thread.Sleep(100);
                }
            });
            thread.IsBackground = true;
            thread.Start();
        }       

        private async Task ServerConnect(ServerStatus serverStatus)
        {
            try
            {
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint point = new IPEndPoint(IPAddress.Parse(serverStatus.ip), serverStatus.port);
                serverStatus.connected = true; // Скажем сразу что коннект установлен - чтоб бесконечно не запускал попытку коннекта из потока запуска подключений
                await socket.ConnectAsync(point);
                lock (serverStatus.locker)
                {
                    //serverStatus.queueSend.Clear();//Мог быть сбой при отправке - при подключении попробуем снова
                    serverStatus.listReceive.Clear();//После коннекта зачистим списки (малоли переподключение) в целом наверно не обязательно
                }
                
                SendMeesage($"{serverStatus.name} подключен");

                _exchange = new Thread(() =>
                {
                    Exchange(socket, serverStatus); // Обмен в отдельном потоке
                });
                _exchange.IsBackground = true;
                _exchange.Start();
            }
            catch 
            {
                serverStatus.connected = false; // Подключится не удалось - Давай ка завново
            }
        }

        private void Exchange(Socket socket, ServerStatus serverStatus) // Процедура получения данных - тут уже в синхроном режиме в своём потоке
        {
            byte[] bytes = new byte[10240];
            try
            {
                while (!_terminate)
                {
                    if (!socket.Connected) break;
                    if ((socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0)) break; // Проверяем ли активно еще соединение

                    if (socket.Available > 0) // Появились данные в буфере
                    {
                        lock (serverStatus.locker)
                        {   // В идеале считывать побайтно и от #90 до #91
                            var cnt = socket.Receive(bytes); // Считываем
                            serverStatus.listReceive.Add(new ListReceive(Encoding.UTF8.GetString(bytes, 0, cnt), 0)); // Записываем сообщение со статусом 0 - не обработано                       
                            SendMeesage($"От {serverStatus.name} принят пакет: {serverStatus.listReceive.Last().value}");
                        }
                    }
                    Thread.Sleep(10);
                }

            }
            catch (SocketException e) 
            {
                SendMeesage($"{serverStatus.name} вызвал ошибку {e.Message}"); 
                //Чето с коннектом - переподключаемся
            }
            finally
            {
                //Закрываем сокет сообщаем что можно заново коннектится
                SendMeesage($"{serverStatus.name} отключён");
                socket.Close();
                serverStatus.connected = false;
            }
        }
    }
}
