﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace TestWork
{
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "TestWorkService";
            serviceInstaller.DisplayName = "TestWorkService";
            serviceInstaller.StartType = ServiceStartMode.Manual;
            serviceInstaller.Description = "TestWorkService";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
