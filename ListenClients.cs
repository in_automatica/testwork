﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Test_Client;
using System.Collections.ObjectModel;
using System.Threading;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace TestWork
{
    public class ListenClients
    {

        private static ListenClients instance;

        public static ListenClients getInstance(Listen settings)
        {
            if (instance == null)
                instance = new ListenClients(settings);
            return instance;
        }

        private Thread _exchange;
        public ListenStatus listenStatus;

        // Это сокет нашего сервера
        private Socket listener;
        private bool isServerRunning;

        protected ListenClients(Listen settings)
        {
            listenStatus = new ListenStatus(settings);
            ServerStart();
        }

        /// <summary>
        /// Сообщения в консоль
        /// </summary>
        private void SendMeesage(string mes)
        {
            Console.WriteLine(mes);
        }

        public void ServerStop()
        {
            isServerRunning = false;
            listener.Close();
        }

        private void ServerStart()
        {
            try
            {
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                isServerRunning = true;
                // Определяем конечную точку, принимаем входящие соединения с любых адресов
                var Point = new IPEndPoint(IPAddress.Any, listenStatus.port);
                // Связываем сокет с конечной точкой
                listener.Bind(Point);
                // Начинаем слушать входящие соединения
                listener.Listen(1);
            }
            catch (Exception ex)
            {
                
            }

            SocketAccepter();
        }

        // Этот метод будет обрабатывать входящие соединения
        private void SocketAccepter()
        {
            Thread th = new Thread(delegate ()
            {
                while (isServerRunning)
                {
                    try
                    {
                        // Ожидаем клиента
                        Socket socket = listener.Accept();
                        Client client = new Client();
                        client.name = socket.RemoteEndPoint.ToString();
                        client.active = true;
                        listenStatus.clients.Add(client);
                        SendMeesage($"Клиент {client.name} подключился");
                        _exchange = new Thread(() =>
                        {
                            Exchange(socket, listenStatus, client); // Обмен в отдельном потоке
                        });
                        _exchange.IsBackground = true;
                        _exchange.Start();
                    }

                    catch (Exception ex)
                    {
                        if (ex.Message.IndexOf("WSACancelBlockingCall") > 0)
                            SendMeesage("Просулшивание порта остановлено");
                    }
                }
            });
            th.Start();
        }

        private void Exchange(Socket socket, ListenStatus listenStatus, Client client) // Процедура отправки данных - тут уже в синхроном режиме в своём потоке
        {
            byte[] bytes = new byte[10240];
            try
            {
                while (isServerRunning)
                {
                    if (!socket.Connected) break;
                    if ((socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0)) break; // Проверяем ли активно еще соединение

                    if (client.queueSend.Count > 0)
                    {
                        lock (listenStatus.locker)
                        {
                            foreach (var item in client.queueSend)
                            {
                                socket.Send(Encoding.UTF8.GetBytes(item));
                                SendMeesage($"Клиенту {client.name} отправлен пакет: {item.Replace("\r\n", "")}");
                            }
                            client.queueSend.Clear();
                        }
                    }
                    Thread.Sleep(10);
                }

            }
            catch (SocketException e)
            {
                SendMeesage($"{client.name} вызвал ошибку {e.Message}");
                //Чето с коннектом - переподключаемся
            }
            finally
            {
                //Закрываем сокет сообщаем что можно заново коннектится
                SendMeesage($"Клиент {client.name} отключился");
                socket.Close();
                client.active = false;
            }
        }

    }


}
