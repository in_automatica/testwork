﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Configuration.Install;
using Test_Client;
using static System.Net.Mime.MediaTypeNames;
using System.Threading;

namespace TestWork
{
    internal class Program
    {
        public static ConnectServers connectServers;
        public static ListenClients listenClients;
        public static Analize analize;
        public const string ServiceName = "TestWorkService";
        public class Service : ServiceBase
        {
            public Service()
            {
                this.CanStop = true;
                this.CanPauseAndContinue = false;
                this.AutoLog = true;
                ServiceName = Program.ServiceName;
            }

            protected override void OnStart(string[] args)
            {
                Program.Start(args);
            }

            protected override void OnStop()
            {
                Program.Stop();
            }
        }

        static void Main(string[] args)
        {
            if (!Environment.UserInteractive)
                // running as service
                using (var service = new Service())
                    ServiceBase.Run(service);
            else
            {
                // running as console app
                Start(args);

                Console.WriteLine("Press any key to stop...");
                Console.ReadKey(true);

                Stop();
            }
        }

        private static void Start(string[] args)
        {
            SettingsApp settings = new SettingsApp($"{AppDomain.CurrentDomain.BaseDirectory}appsettings.json");
            connectServers = ConnectServers.getInstance(settings.Servers); //Ток один экземпляр этого класса можно
            listenClients = ListenClients.getInstance(settings.Listen); //Аналогично
            analize = Analize.getInstance(connectServers.serverStatus, listenClients.listenStatus); //Аналогично
        }

        private static void Stop()
        {
            connectServers.Disconnect();
            listenClients.ServerStop();
            analize.Stop();
            Thread.Sleep(1000); //Дадим время завершится всем потокам (они конечно итак фоновые)
        }
    }
}
